from mysql.connector import connect
from faker.factory import Factory


class database:
    def __init__(self):
        try:
            self.db = connect(host='localhost',
                              database='final1',
                              user='root',
                              password='adminadmin')
        except Exception as e:
            print(e)

    def showAllUsers(self):
        try:
            cursor = self.db.cursor()
            query = 'select * from users'
            cursor.execute(query)
            result = cursor.fetchall()
            print(result)

        except Exception as e:
            print(e)

    def showUser(self, id):  # by id
        try:
            query = "select * from users where id={0}".format(
                id)
            cursor = self.db.cursor()
            cursor.execute(query)
            result = cursor.fetchall()
            print(result)

        except Exception as e:
            print(e)

    def deleteUser(self, id):
        try:
            query = "delete from users where id={0}".format(
                id)
            cursor = self.db.cursor()
            cursor.execute(query)
            cursor.close()
            self.db.commit()
            print("Data berhasil dihapus")
        except Exception as e:
            print(e)

    def createDummy(self):
        try:
            Faker = Factory.create
            fake = Faker()
            firstName = fake.first_name()
            lastName = fake.last_name()
            phone = fake.msisdn()
            dob = fake.date()
            email = fake.email()
            dummyUser = {
                'firstName': firstName,
                'lastName': lastName,
                'phone': phone,
                'email': (firstName+lastName+'@'+fake.domain_name()).lower(),
                'dob': dob,
                'username': firstName+lastName
            }

            column = str([i for i in dummyUser.keys()]).replace(
                '\'', '').replace('[', '(').replace(']', ')')
            values = tuple(dummyUser.values())
            query = 'insert into users {0} values {1}'.format(
                column, values)
            cursor = self.db.cursor()
            cursor.execute(query)
            cursor.close()
            self.db.commit()
            print('Data berhasil ditambahkan')

        except Exception as e:
            print(e)

    def createUser(self, **data):
        try:
            column = str([i for i in data.keys()]).replace(
                '\'', '').replace('[', '(').replace(']', ')')
            values = tuple(data.values())
            query = 'insert into users {0} values {1}'.format(
                column, values)
            cursor = self.db.cursor()
            cursor.execute(query)
            cursor.close()
            self.db.commit()
            print('Data berhasil ditambahkan')

        except Exception as e:
            print(e)

    def updateUser(self, id, **data):
        try:
            columns = data.keys()
            values = data.values()
            query = []
            for i, j in zip(columns, values):
                v = "update users set {0} = '{1}'  where id = {2}".format(
                    i, j, id)
                query.append(v)

            cursor = self.db.cursor()
            [cursor.execute(q) for q in query]
            cursor.close()
            self.db.commit()
            print('Data berhasil diupdate')

        except Exception as e:
            print(e)


# db = database()
# id = 1
# data = {
#     'email': 'bejo@bejo.com'
# }
# db.updateUser(id, **data)
