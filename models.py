from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy import Column, Integer, String


engine = create_engine(
    'mysql+mysqlconnector://root:adminadmin@localhost:3306/final1', echo=True)
Session = sessionmaker(bind=engine)
session = Session()

if session:
    print('Connection Success')


Base = declarative_base()


class Customers(Base):
    __tablename__ = 'user'
    id = Column(Integer, primary_key=True)
    firstName = Column(String)
    lastName = Column(String)
    username = Column(String)
    email = Column(String)
    address = Column(String)
    phone = Column(String)


result = session.query(Customers).all()

for row in result:
    print(row.id, row.username, row.email)

result = session.query(Customers).filter(Customers.id == 2)
